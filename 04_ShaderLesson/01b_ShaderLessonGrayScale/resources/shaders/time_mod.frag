#ifdef GL_ES
#	if __VERSION__ >= 130
#		define varying in
		out vec4 vgl_FragColor;
#		define texture2D texture
#	else
#		define vgl_FragColor gl_FragColor
#	endif
	precision mediump float;
#else
#	if __VERSION__ >= 130
#		define varying in
		out vec4 vgl_FragColor;
#		define texture2D texture
#	else
#		define vgl_FragColor gl_FragColor
#	endif
#endif

varying vec2 texCoord;

uniform sampler2D theTexture;

void main(void)
{
	vec4 texfrag = texture2D(theTexture, texCoord);
	//      float gray = (texfrag.r + texfrag.g + texfrag.b)/3.0;
	float gray = 0.3*texfrag.r + 0.59*texfrag.g + 0.11*texfrag.b;
	vgl_FragColor = vec4(gray, gray, gray, texfrag.a);
}
