#ifndef _SDL_GPU_H__
#include "SDL_gpu.h"
#endif

#define MAX_INJECTION_STRING_LENGTH 1024
#define MAX_VERSION_STRING_LENGTH 32


/**
 Loads a shader and prepends version/compatibility info before compiling it.
 Normally, you can just use GPU_LoadShader() for shader source files or GPU_CompileShader() for strings.
 This was adapted from a SDL_gpu example where
 'some hardware (certain ATI/AMD cards) does not let you put non-#version preprocessing at the top of the file'
 'Therefore, I need to prepend the version info here so I can support both GLSL and GLSLES with one shader file.'
 This extends the idea slightly and generate the version based on the current GL version in use.
 Milage may vary since OpenGL makes it a habit to make hard breaking changes every version.
 For the most future-proof code, you are probably better off always initializing a specific OpenGL version
 and loading a version tailored shader yourself instead of using this function.
 @param shader_type The shader type, typically GPU_VERTEX_SHADER or GPU_FRAGMENT_SHADER.
 @param input_shader_string A string containing your shader program.
 @return Returns the shader id.
 */
Uint32 GPU_EXT_LoadVersionFlexibleShaderFromString(GPU_ShaderEnum shader_type, const char* input_shader_string)
{
	char injected_header[MAX_INJECTION_STRING_LENGTH];
	char version_characters[MAX_VERSION_STRING_LENGTH];
	
	Uint32 shader_id;
	char* shader_source;
	size_t header_size;
	Sint64 source_shader_strlen;
	size_t final_source_size;
	GPU_Renderer* gpu_renderer = GPU_GetCurrentRenderer();
	
	SDL_strlcpy(injected_header, "#version ", MAX_INJECTION_STRING_LENGTH);
	


/*
	SDL_Log("shader version: %d", gpu_renderer->max_shader_version);
*/
	if(gpu_renderer->shader_language == GPU_LANGUAGE_GLSL)
	{
		if(gpu_renderer->max_shader_version >= 120)
		{
			SDL_itoa(gpu_renderer->max_shader_version, version_characters, 10);
			
			SDL_strlcat(injected_header, version_characters, MAX_INJECTION_STRING_LENGTH);
			SDL_strlcat(injected_header, "\n", MAX_INJECTION_STRING_LENGTH);
		}
		else
		{
			// Maybe this is good enough?
			SDL_strlcat(injected_header, "110\n", MAX_INJECTION_STRING_LENGTH);
		}
	}
	else if(gpu_renderer->shader_language == GPU_LANGUAGE_GLSLES)
	{
		SDL_itoa(gpu_renderer->max_shader_version, version_characters, 10);
		SDL_strlcat(injected_header, version_characters, MAX_INJECTION_STRING_LENGTH);
		/* needs a ' es' at the end of the string */
		if(gpu_renderer->max_shader_version >= 300)
		{
			SDL_strlcat(injected_header, " es", MAX_INJECTION_STRING_LENGTH);
		}
		SDL_strlcat(injected_header, "\n", MAX_INJECTION_STRING_LENGTH);
		SDL_strlcat(injected_header, "precision mediump int;\n", MAX_INJECTION_STRING_LENGTH);
		SDL_strlcat(injected_header, "precision mediump float;\n", MAX_INJECTION_STRING_LENGTH);
	}
	/* Reset the line count so if an error shows up, the user isn't confused by the line numbers being off because they don't see the injected code. */
	/* Note: 0 is not a legal value and can trip up some drivers. We must use 1, so the line count may be off-by-one. */
	SDL_strlcat(injected_header, "#line 1\n", MAX_INJECTION_STRING_LENGTH);
	

	header_size = SDL_strlen(injected_header);
	
	/* Get source shader length */
	source_shader_strlen = SDL_strlen(input_shader_string);
	
	/* Allocate source buffer */
	final_source_size = header_size + source_shader_strlen + 1;
	shader_source = (char*)SDL_malloc(sizeof(char)*final_source_size);
	
	/* Prepend header */
	SDL_strlcpy(shader_source, injected_header, final_source_size);
	
	/* Append original shader */
	SDL_strlcat(shader_source, input_shader_string, final_source_size);
	
	/* Compile the shader */
	shader_id = GPU_CompileShader(shader_type, shader_source);
	
	/* Clean up */
	SDL_free(shader_source);
	
	return shader_id;
}


/**
Loads a shader and prepends version/compatibility info before compiling it.
Normally, you can just use GPU_LoadShader() for shader source files or GPU_CompileShader() for strings.
This was adapted from a SDL_gpu example where
'some hardware (certain ATI/AMD cards) does not let you put non-#version preprocessing at the top of the file'
'Therefore, I need to prepend the version info here so I can support both GLSL and GLSLES with one shader file.'
This extends the idea slightly and generate the version based on the current GL version in use.
Milage may vary since OpenGL makes it a habit to make hard breaking changes every version.
For the most future-proof code, you are probably better off always initializing a specific OpenGL version
and loading a version tailored shader yourself instead of using this function.
@param shader_type The shader type, typically GPU_VERTEX_SHADER or GPU_FRAGMENT_SHADER.
@param file_name The shader file to load from. This should typically include the path returned by BlurrrCore_GetResourceDirectoryString() or SDL_GetBasePath().
@return Returns the shader id.
*/
Uint32 GPU_EXT_LoadVersionFlexibleShaderFromFile(GPU_ShaderEnum shader_type, const char* file_name)
{
	SDL_RWops* rw_ops;
	Uint32 shader_id;
	char* shader_source;
	Sint64 file_size;
	size_t source_string_size;
	
	
	/* Open file */
	rw_ops = SDL_RWFromFile(file_name, "r");
	if(rw_ops == NULL)
	{
		GPU_PushErrorCode("GPU_EXT_LoadVersionFlexibleShaderFromFile", GPU_ERROR_FILE_NOT_FOUND, "Shader file \"%s\" not found", file_name);
		return 0;
	}
	
	/* Get file size */
	file_size = SDL_RWseek(rw_ops, 0, SEEK_END);
	SDL_RWseek(rw_ops, 0, SEEK_SET);
	
	/* Allocate source buffer */
	source_string_size = file_size + 1;
	shader_source = (char*)SDL_malloc(sizeof(char)*source_string_size);
	
	/* Read in source code */
	SDL_RWread(rw_ops, shader_source, 1, file_size);
	shader_source[file_size] = '\0';
/*
	SDL_Log("shader_source:\n%s", shader_source);
*/
	/* Compile the shader */
	shader_id = GPU_EXT_LoadVersionFlexibleShaderFromString(shader_type, shader_source);
	
	/* Clean up */
	SDL_free(shader_source);
	SDL_RWclose(rw_ops);
	
	return shader_id;
}


void GPU_EXT_UpdateLogicalViewport(GPU_Target* gpu_target, int logical_width, int logical_height, int scale_policy)
{
	int w = 1, h = 1;
	float want_aspect;
	float real_aspect;
	float scale;
	GPU_Rect viewport;
	/* 0 is for letterbox, 1 is for overscan */
	//	int scale_policy = 0;
	
	// scale_policy=0 => letterbox
	// scale_policy=0 => overscan
	
	// On iPad, taking the gpu_target->w and ->h doesn't work because it has the original requested window size instead of the real fullscreen size.
	if(gpu_target->context)
	{
		w = gpu_target->context->window_w;
		h = gpu_target->context->window_h;
	}
	else
	{
		w = gpu_target->w;
		h = gpu_target->h;
	}
	
	want_aspect = (float)logical_width / logical_height;
	real_aspect = (float)w / h;
	
	/* Clear the scale because we're setting viewport in output coordinates */
	//	SDL_RenderSetScale(renderer, 1.0f, 1.0f);
	
	if (SDL_fabs(want_aspect-real_aspect) < 0.0001) {
		/* The aspect ratios are the same, just scale appropriately */
		scale = (float)w / logical_width;
		//		GPU_SetViewport(gpu_target, GPU_MakeRect(0, 0, logical_width, logical_height));
		// This gets hit on startup. logical wasn't working right.
		GPU_SetViewport(gpu_target, GPU_MakeRect(0, 0, w, h));
		
	} else if (want_aspect > real_aspect) {
		if (scale_policy == 1) {
			/* We want a wider aspect ratio than is available -
			 zoom so logical height matches the real height
			 and the width will grow off the screen
			 */
			scale = (float)h / logical_height;
			viewport.y = 0;
			viewport.h = h;
			viewport.w = (int)SDL_ceil(logical_width * scale);
			viewport.x = (w - viewport.w) / 2;
			GPU_SetViewport(gpu_target, viewport);
		} else {
			/* We want a wider aspect ratio than is available - letterbox it */
			scale = (float)w / logical_width;
			viewport.x = 0;
			viewport.w = w;
			viewport.h = (int)SDL_ceil(logical_height * scale);
			viewport.y = (h - viewport.h) / 2;
			GPU_SetViewport(gpu_target, viewport);
		}
	} else {
		if (scale_policy == 1) {
			/* We want a narrower aspect ratio than is available -
			 zoom so logical width matches the real width
			 and the height will grow off the screen
			 */
			scale = (float)w / logical_width;
			viewport.x = 0;
			viewport.w = w;
			viewport.h = (int)SDL_ceil(logical_height * scale);
			viewport.y = (h - viewport.h) / 2;
			GPU_SetViewport(gpu_target, viewport);
		} else {
			/* We want a narrower aspect ratio than is available - use side-bars */
			scale = (float)h / logical_height;
			viewport.y = 0;
			viewport.h = h;
			viewport.w = (int)SDL_ceil(logical_width * scale);
			viewport.x = (w - viewport.w) / 2;
			GPU_SetViewport(gpu_target, viewport);
		}
	}
	
	
}


void GPU_EXT_GetLogicalCoords(GPU_Target* gpu_target, float* x, float* y, float displayX, float displayY, int logical_width, int logical_height, int scale_policy)
{
	if(gpu_target == NULL)
	{
		return;
	}
	
	int w = 1, h = 1;
	float want_aspect;
	float real_aspect;
	float scale;
	GPU_Rect viewport;
	float adjusted_x;
	float adjusted_y;
	
	/* 0 is for letterbox, 1 is for overscan */
	//	int scale_policy = 0;
	
	// scale_policy=0 => letterbox
	// scale_policy=0 => overscan
	
	//	w = gpu_target->w;
	//	h = gpu_target->h;
	
	w = gpu_target->context->window_w;
	h = gpu_target->context->window_h;
	
	want_aspect = (float)logical_width / logical_height;
	//	real_aspect = (float)gpu_target->w / gpu_target->h;
	real_aspect = (float)w / h;
	
	/* Clear the scale because we're setting viewport in output coordinates */
	//	SDL_RenderSetScale(renderer, 1.0f, 1.0f);
	
	if (SDL_fabs(want_aspect-real_aspect) < 0.0001) {
		/* The aspect ratios are the same, just scale appropriately */
		scale = (float)w / logical_width;
		//		GPU_SetViewport(gpu_target, GPU_MakeRect(0, 0, logical_width, logical_height));
		// This gets hit on startup. logical wasn't working right.
		viewport = GPU_MakeRect(0, 0, w, h);
	} else if (want_aspect > real_aspect) {
		if (scale_policy == 1) {
			/* We want a wider aspect ratio than is available -
			 zoom so logical height matches the real height
			 and the width will grow off the screen
			 */
			scale = (float)h / logical_height;
			viewport.y = 0;
			viewport.h = h;
			viewport.w = (int)SDL_ceil(logical_width * scale);
			viewport.x = (w - viewport.w) / 2;
		} else {
			/* We want a wider aspect ratio than is available - letterbox it */
			scale = (float)w / logical_width;
			viewport.x = 0;
			viewport.w = w;
			viewport.h = (int)SDL_ceil(logical_height * scale);
			viewport.y = (h - viewport.h) / 2;
		}
	} else {
		if (scale_policy == 1) {
			/* We want a narrower aspect ratio than is available -
			 zoom so logical width matches the real width
			 and the height will grow off the screen
			 */
			scale = (float)w / logical_width;
			viewport.x = 0;
			viewport.w = w;
			viewport.h = (int)SDL_ceil(logical_height * scale);
			viewport.y = (h - viewport.h) / 2;
		} else {
			/* We want a narrower aspect ratio than is available - use side-bars */
			scale = (float)h / logical_height;
			viewport.y = 0;
			viewport.h = h;
			viewport.w = (int)SDL_ceil(logical_width * scale);
			viewport.x = (w - viewport.w) / 2;
		}
	}
	
	
	
	adjusted_x = ((float)displayX - viewport.x) / scale;
	adjusted_y = ((float)displayY - viewport.y) / scale;
	/* Need to invert to match Cartesian coordinates if mode 1 */
	if(1 == GPU_GetCoordinateMode())
	{
		adjusted_y = logical_height - adjusted_y;
	}
	
	/*
	 adjusted_x / scale;
	 
	 event->motion.x -= renderer->viewport.x;
	 event->motion.y -= renderer->viewport.y;
	 event->motion.x = (int)(event->motion.x / renderer->scale.x);
	 event->motion.y = (int)(event->motion.y / renderer->scale.y);
	 */
	
	
	if(gpu_target->context != NULL)
	{
		if(x != NULL)
		{
			//			*x = (displayX*target->w)/target->context->window_w;
			//			*x = (displayX*target->w)/target->viewport.w;
			*x = adjusted_x;
			
		}
		if(y != NULL)
		{
			//			*y = (displayY*target->h)/target->context->window_h;
			//			*y = (displayY*target->h)/target->viewport.h;
			
			*y = adjusted_y;
			
		}
	}
	else if(gpu_target->image != NULL)
	{
		if(x != NULL)
			*x = (displayX*gpu_target->w)/gpu_target->image->w;
		if(y != NULL)
			*y = (displayY*gpu_target->h)/gpu_target->image->h;
	}
	else
	{
		if(x != NULL)
			*x = displayX;
		if(y != NULL)
			*y = displayY;
	}
	
	
	
}

