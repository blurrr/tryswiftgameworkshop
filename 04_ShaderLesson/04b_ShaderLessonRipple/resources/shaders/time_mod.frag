#ifdef GL_ES
#	if __VERSION__ >= 130
#		define varying in
		out vec4 vgl_FragColor;
#		define texture2D texture
#	else
#		define vgl_FragColor gl_FragColor
#	endif
	precision mediump float;
#else
#	if __VERSION__ >= 130
#		define varying in
		out vec4 vgl_FragColor;
#		define texture2D texture
#	else
#		define vgl_FragColor gl_FragColor
#	endif
#endif

varying vec4 color;
varying vec2 texCoord;

uniform sampler2D theTexture;
uniform float time;


uniform vec2 mousePoint;
uniform vec2 logicalScreenSize;
uniform vec2 imageSize;
uniform vec2 pixelSize;
uniform float radius;


void main()
{
//	vec2 logicalScreenSize = vec2(1024, 768);

	// Radius is expected to be between [0,1].
	// Think of it as the the percentage of the image size to distort.
//	float radius = .5;


	// This code does its math in normalized (fractional) values betwen 0 and 1.
	// Our mouse points and image and screen sizes are in large whole integer values (e.g. 1024x768).
	// So we need to normalize these values into the range of 0 and 1.
	// Additionally, this fragment shader, and particularly texCoord is relative to the specific image being rendered,
	// not the whole screen. So we need to make sure to normalize our values to be relative to the image.

	// So to the compute the center point of the distortion, we need to convert our mouse coordinate
	// to be in the range of [0,1] where 0 is the begining of the texture and 1 is the end.

	// So, first we know we centered our image in the window.
	// So if we subtract the image size from our logical screen size, we get the amount of extra spaces on the edges.
	// Since the image is centered, there are edges on both sides, so divide by two to get the size of one of the padding edges.
	vec2 offset = (logicalScreenSize - imageSize) / 2.0;

	// Now that we know how much lead padding there is between the starting edge and the image,
	// we subtract the two to convert out of the screen space coordinates and into the image space coordinates.
	// Now we divide this converted mouse point by the image size to get its percent [0,1] location relative to the image.
	vec2 center = (mousePoint - offset) / imageSize;


	// texCoord is the texture cordinate we are going to look up to render this fragment.
	// texCoord can range from [0,1], however, if the texture is a non-power of two, there is an edge case.
	// We want our mousePoint and texCoord to line up, but for a non-power-of-two texture, the range might stop short
	// (e.g. [0, 0.75] for 768, because 768 is .75 of the next power-of-two).
	// See the main.c code for more detail. We already adjusted the mouse coordinates to handle this edge case,
	// so there is actually nothing special to see here.
	vec2 this_pixel = texCoord;

	// 'radius' was specified as a fraction of
	// the image dimensions. Since the image
	// might be non-square we must "square it up".
	vec2 r = radius * pixelSize;

	//	r=r*r;
	r *= 0.5 * (imageSize.x + imageSize.y);


	// This "spherize" distortion originated from:
	// https://graphics.stanford.edu/wikis/cs148-09/Assignment5
	
	// Compute the distance from this pixel
	// to the center of the circle
	vec2 d = (this_pixel - center) / r;
	float dist = length(d);
	
	if( dist < 1. )
	{
		// For points within the circle, we
		// distort their distance as if the
		// image had been stretched over a
		// half-sphere.
		vec2 dir = d / dist;

		this_pixel = this_pixel + (.02)*cos(dist*100.0 - 10.0*time);
	}
	vgl_FragColor = texture2D(theTexture, this_pixel);
}
