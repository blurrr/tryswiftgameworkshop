
/*
// Recommend trying to avoid Foundation because it is unfinished and big 
// (and may not be included yet in your Swift distribution).
// SDL and BlurrrCore provide most of the C library so it can be avoided.
// Modify your CMakeBlurrrDefaultOptionsOverride.cmake to include these 
// libraries or not. That file controls whether the libraries will be 
// copied into your app bundle.

#if os(Linux)
    import Glibc
#else
    import Darwin
#endif

import Foundation

*/
let MILLESECONDS_PER_FRAME : UInt32 = 16  /* about 60 frames per second */

var g_VirtualScreenWidth : Int32 = 1280
var g_VirtualScreenHeight : Int32 = 720
let REAL_SCREEN_WIDTH : Int32 = 1280
let REAL_SCREEN_HEIGHT : Int32 = 720



var g_appDone = false;
var g_mainGPUTarget : UnsafeMutablePointer<GPU_Target>? = nil;
var g_mainImage : UnsafeMutablePointer<GPU_Image>? = nil;
var g_vertexShaderID : UInt32 = 0;
var g_fragmentShaderID : UInt32 = 0;
var g_programShaderID : UInt32 = 0




var g_distortionCenter: [CFloat] = [0.0, 0.0];
var g_radius : Float = 0.5;



var g_baseTime : UInt32 = 0
var g_lastFrameTime : UInt32 = 0
var g_gameClock : OpaquePointer? = nil;

var g_isMouseButtonDown = false;


/* HACK for Android: This is a workaround for Android which doesn't actually
   quit properly. Instead it leaves global and static variables in memory,
   and when the program is relaunched, it doesn't reinitialize these variables.
   So they contain garbage values from the previous run.
   This function force reinitializes global variables.
*/
func InitGlobalVariables()
{
	g_appDone = false
	g_mainGPUTarget = nil
	g_mainImage = nil
	g_vertexShaderID = 0
	g_fragmentShaderID = 0
	g_programShaderID = 0

	
	g_distortionCenter = [0.0, 0.0];
	g_radius = 0.5

	g_isMouseButtonDown = false;
	
	g_baseTime = 0
	g_lastFrameTime = 0
	g_gameClock = nil;

}


func SDL_LogV(_ fmt: String, _ arguments: CVarArg...) -> Void
{
	return withVaList(arguments)
	{
		SDL_LogMessageV(
			Int32(SDL_LOG_CATEGORY_APPLICATION),
			SDL_LOG_PRIORITY_INFO,
			fmt,
			$0
		)
	}
}

func SDL_Log(_ strings: String...) -> Void
{
	var whole_string: String = "";
	for str in strings
	{
		whole_string = whole_string + str;
	}
	print(whole_string);
	SDL_LogV(whole_string);
}

func GetResourceDirectoryString() -> String
{
	let base_path = BlurrrPath_CreateResourceDirectoryString();
	if(nil == base_path)
	{
		return ""
	}
	else
	{
		let return_path = String(cString: base_path!);
		BlurrrCore_Free(base_path);
		return return_path;
	}
}

func TemplateHelper_ToggleFullScreen(_ the_target:UnsafeMutablePointer<GPU_Target>?) -> Bool
{
	let is_currently_fullscreen = GPU_GetFullscreen();
	let ret_val: Bool;

	if(is_currently_fullscreen)
	{
		ret_val = GPU_SetFullscreen(false, false);
	}
	else
	{
		ret_val = GPU_SetFullscreen(true, true);
	}

	/* The values should be different if it worked. So it is an error if they are the same. */
	if(ret_val == is_currently_fullscreen)
	{
		return false;
	}

	/* Must update viewport first, then set virtual resolution or it doesn't work right. */
	GPU_EXT_UpdateLogicalViewport(the_target, g_VirtualScreenWidth, g_VirtualScreenHeight, 0);
	GPU_SetVirtualResolution(the_target, Uint16(g_VirtualScreenWidth), Uint16(g_VirtualScreenHeight));

    return true;
}


func main_loop()
{
	let start_frame : UInt32 = BlurrrTicker_UpdateTime(g_gameClock);
	let current_time = start_frame;
	let delta_time = current_time - g_lastFrameTime;
	var the_result : CInt = 0;

	let fps_real_start_frame_time : UInt64 = SDL_GetPerformanceCounter();




	// When ALmixer is compiled as multithreaded, this call becomes a no-op.
	// But in case you have a platform that is single-threaded, you want to keep this call.
	ALmixer_Update();


	repeat
	{
		var the_event : SDL_Event = SDL_Event();
		
		the_result = SDL_PollEvent(&the_event)
		if(the_result > 0)
		{
			let event_type: SDL_EventType = SDL_EventType(the_event.type);
			
			switch(event_type.rawValue)
			{
				case SDL_MOUSEMOTION.rawValue:
					if(g_isMouseButtonDown)
					{
					}
					break;
				case SDL_MOUSEBUTTONDOWN.rawValue:
					g_isMouseButtonDown = true;
				case SDL_MOUSEBUTTONUP.rawValue:
					g_isMouseButtonDown = false;
				
				case SDL_MOUSEWHEEL.rawValue:
					break;

				case SDL_MULTIGESTURE.rawValue:
					if(SDL_fabs(Double(the_event.mgesture.dDist) ) > 0.002)
					{
					}
				
				
				case SDL_QUIT.rawValue:
					g_appDone = true;
				case SDL_APP_TERMINATING.rawValue:
					g_appDone = true;
				
				
				case SDL_KEYDOWN.rawValue:
					if(SDL_Keycode(SDLK_AC_BACK) == the_event.key.keysym.sym)
					{
						g_appDone = true;
					}
					else if(SDL_Keycode(SDLK_ESCAPE) == the_event.key.keysym.sym)
					{
						g_appDone = true;
					}

					else if(SDL_Keycode(SDLK_RETURN) == the_event.key.keysym.sym)
					{
						/* CMD-Enter (Mac) or Ctrl-Enter (everybody else) to toggle fullscreen */
						#if os(OSX)
							if( (UInt16(the_event.key.keysym.mod) & UInt16(KMOD_GUI)) > 0)
							{
								_ = TemplateHelper_ToggleFullScreen(g_mainGPUTarget);
							}
						#else
							if( (UInt16(the_event.key.keysym.mod) & UInt16(KMOD_ALT)) > 0)
							{
								_ = TemplateHelper_ToggleFullScreen(g_mainGPUTarget);
							}
						#endif
					}
					/* CMD-F (Mac) or Ctrl-F (everybody else) to toggle fullscreen */
					else if(SDL_Keycode(SDLK_f) == the_event.key.keysym.sym)
					{
						#if os(OSX)
							if( (UInt16(the_event.key.keysym.mod) & UInt16(KMOD_GUI)) > 0)
							{
								_ = TemplateHelper_ToggleFullScreen(g_mainGPUTarget);
							}
						#else
							if( (UInt16(the_event.key.keysym.mod) & UInt16(KMOD_ALT)) > 0)
							{
								_ = TemplateHelper_ToggleFullScreen(g_mainGPUTarget);
							}
						#endif
					}


					else if( (SDL_Keycode(SDLK_EQUALS) == the_event.key.keysym.sym) || (SDL_Keycode(SDLK_PLUS) == the_event.key.keysym.sym))
					{

					}
					else if(SDL_Keycode(SDLK_MINUS) == the_event.key.keysym.sym)
					{

					}

				case SDL_WINDOWEVENT.rawValue:
					switch(the_event.window.event)
					{
						case UInt8(SDL_WINDOWEVENT_HIDDEN.rawValue):
							break;

						case UInt8(SDL_WINDOWEVENT_MINIMIZED.rawValue):
							break;
						case UInt8(SDL_WINDOWEVENT_RESIZED.rawValue):
							GPU_SetWindowResolution(Uint16(the_event.window.data1), Uint16(the_event.window.data2));
							// Must update viewport first, then set virtual resolution or it doesn't work right.
							GPU_EXT_UpdateLogicalViewport(g_mainGPUTarget, g_VirtualScreenWidth, g_VirtualScreenHeight, 0);
							GPU_SetVirtualResolution(g_mainGPUTarget, Uint16(g_VirtualScreenWidth), Uint16(g_VirtualScreenHeight));
							break;
						default:
							break;
					}
					break;	
					
				default:
					break;
			}
		}
	
		
	} while(the_result > 0)
	
	
	Update(g_baseTime, current_time: current_time, delta_time: delta_time)

	Render(g_baseTime, current_time: current_time, delta_time: delta_time)

	g_lastFrameTime = current_time

	/* figure out how much time we have left, and then sleep */
	let fps_real_end_frame_time : UInt64 = SDL_GetPerformanceCounter();
	let fps_real_diff_time_in_ms : UInt32 = UInt32(
		Float64(fps_real_end_frame_time - fps_real_start_frame_time)
			/ Float64(SDL_GetPerformanceFrequency()*1000) + 0.5)

	var sleep_delay = MILLESECONDS_PER_FRAME - fps_real_diff_time_in_ms;
	if(sleep_delay <= 0)
	{
		sleep_delay = 0;
	}
	else if(sleep_delay > MILLESECONDS_PER_FRAME)
	{
		sleep_delay = MILLESECONDS_PER_FRAME;
	}

	// Try to not suck up all the CPU	
	SDL_Delay(sleep_delay);
}

func Update(_ base_time:Uint32, current_time:Uint32, delta_time:Uint32)
{
	

}


func Render(_ base_time:Uint32, current_time:Uint32, delta_time:Uint32)
{
	guard g_mainImage != nil else
	{
		return;
	}
	
	GPU_Clear(g_mainGPUTarget);
	
	
	GPU_Blit(g_mainImage, nil, g_mainGPUTarget, Float32(g_VirtualScreenWidth)/2.0, Float32(g_VirtualScreenHeight)/2.0);

	
	GPU_Flip(g_mainGPUTarget);		
}

let TemplateHelper_HandleAppEvents : @convention(c) (UnsafeMutableRawPointer?, UnsafeMutablePointer<SDL_Event>?) -> CInt =
{
	(user_data, the_event) -> CInt in
	
	//	var event_type = SDL_EXT_EventGetTypeFromPtr(the_event);
	var event_type:SDL_EventType = SDL_EventType(the_event!.pointee.type);
	
	switch(event_type)
	{
	case SDL_APP_TERMINATING:
		/* Terminate the app.
		Shut everything down before returning from this function.
		*/
		
		return 0;
	case SDL_APP_LOWMEMORY:
		/* You will get this when your app is paused and iOS wants more memory.
		Release as much memory as possible.
		*/
		return 0;
	case SDL_APP_WILLENTERBACKGROUND:
		/* Prepare your app to go into the background.  Stop loops, etc.
		This gets called when the user hits the home button, or gets a call.
		*/
		
		return 0;
	case SDL_APP_DIDENTERBACKGROUND:
		/* This will get called if the user accepted whatever sent your app to the background.
		If the user got a phone call and canceled it, you'll instead get an    SDL_APP_DIDENTERFOREGROUND event and restart your loops.
		When you get this, you have 5 seconds to save all your state or the app will be terminated.
		Your app is NOT active at this point.
		*/
		return 0;
	case SDL_APP_WILLENTERFOREGROUND:
		/* This call happens when your app is coming back to the foreground.
		Restore all your state here.
		*/
		return 0;
	case SDL_APP_DIDENTERFOREGROUND:
		/* Restart your loops here.
		Your app is interactive and getting CPU again.
		*/
		return 0;
	default:
		/* No special processing, add it to the event queue */
		return 1;
	}
}


func LoadTextures()
{
	let base_path = GetResourceDirectoryString();

	do
	{
		// "Lummenfelsen", Heligoland, by Hartmut Josi Bennöhr, Creative Commons Attribution-Share Alike 3.0 Unported
		// https://commons.wikimedia.org/wiki/File:Helgoland_Lummenfelsen_22067.JPG
		
		let resource_file_path = base_path + "sediment_layer.jpg"
		let image_surface : UnsafeMutablePointer<SDL_Surface>? = IMG_Load(resource_file_path);
		if(nil == image_surface)
		{
			SDL_Log("Failed to load image");
		}
		else
		{
			let the_image = GPU_CopyImageFromSurface(image_surface);
			SDL_FreeSurface(image_surface);
			if(nil == the_image)
			{
				SDL_Log("could not load image");
			}
			else
			{
			
				g_mainImage = the_image;
				g_VirtualScreenWidth = Int32(the_image!.pointee.w);
				g_VirtualScreenHeight = Int32(the_image!.pointee.h);
			
				GPU_EXT_UpdateLogicalViewport(g_mainGPUTarget, g_VirtualScreenWidth, g_VirtualScreenHeight, 0);
				GPU_SetVirtualResolution(g_mainGPUTarget, Uint16(g_VirtualScreenWidth), Uint16(g_VirtualScreenHeight));

			}
		}
	}
}

func UnloadTextures()
{
	GPU_FreeImage(g_mainImage);
	g_mainImage = nil;
}


func LoadShaders()
{
	let base_path = GetResourceDirectoryString();

	let resource_file_path_time_mod_vert = base_path + "shaders/time_mod.vert"
	let vertex_shader_id : UInt32 = GPU_EXT_LoadVersionFlexibleShaderFromFile(GPU_VERTEX_SHADER, resource_file_path_time_mod_vert);
	if(0 == vertex_shader_id)
	{
		SDL_Log("Vertex shader: \(resource_file_path_time_mod_vert)  could not be loaded: \(GPU_GetShaderMessage())");
	}

	let resource_file_path_time_mod_frag = base_path + "shaders/time_mod.frag"
	let fragment_shader_id : UInt32 = GPU_EXT_LoadVersionFlexibleShaderFromFile(GPU_FRAGMENT_SHADER, resource_file_path_time_mod_frag);
	if(0 == fragment_shader_id)
	{
		SDL_Log("Fragment shader: \(resource_file_path_time_mod_frag)  could not be loaded: \(GPU_GetShaderMessage())");
	}


	let program_shader_id : UInt32 = GPU_LinkShaders(vertex_shader_id, fragment_shader_id);
	if(0 == program_shader_id)
	{
		SDL_Log("Program shader object failed to link: \(GPU_GetShaderMessage())");
	}

	var gpu_shader_block : GPU_ShaderBlock = GPU_LoadShaderBlock(program_shader_id, "gpu_Vertex", "gpu_TexCoord", "gpu_Color", "gpu_ModelViewProjectionMatrix");
	GPU_ActivateShaderProgram(program_shader_id, &gpu_shader_block);

	g_vertexShaderID = vertex_shader_id;
	g_fragmentShaderID = fragment_shader_id;
	g_programShaderID = program_shader_id;
}

func FreeShaders()
{
	GPU_FreeShaderProgram(g_programShaderID);
	GPU_FreeShader(g_fragmentShaderID);
	GPU_FreeShader(g_vertexShaderID);
	g_programShaderID = 0;
	g_fragmentShaderID = 0;
	g_vertexShaderID = 0;
}

func InitUniforms()
{


}

func CleanUp()
{
	FreeShaders();
	UnloadTextures();

	BlurrrTicker_Free(g_gameClock);
	g_gameClock = nil;
}




// This function is the official starting point of the Swift program.
func BlurrrMain() -> Int32
{
	// hack for android
	InitGlobalVariables()

	if(SDL_Init(Uint32(SDL_INIT_VIDEO)) < 0)
	{
		print("Could not initialize SDL");
	}
	if(IMG_Init(Int32(IMG_INIT_JPG.rawValue | IMG_INIT_PNG.rawValue | IMG_INIT_TIF.rawValue)) < 0)
	{
		print("Could not initialize SDL_image");
	}
	if(TTF_Init() < 0)
	{
		print("Could not initialize SDL_ttf");
	}
	
	if(ALmixer_Init(0, 0, 0) == ALboolean(AL_FALSE))
	{
		print("Could not initialize ALmixer");
	}
	
	
	let gpu_target: UnsafeMutablePointer<GPU_Target>? = GPU_Init(UInt16(g_VirtualScreenWidth), UInt16(g_VirtualScreenHeight),
		UInt32(GPU_DEFAULT_INIT_FLAGS)
		| UInt32(SDL_WINDOW_RESIZABLE.rawValue)
//		| UInt32(SDL_WINDOW_FULLSCREEN.rawValue)
	);
	if(nil == gpu_target)
	{
		SDL_Log("GPU_Init failed");
		return -1;
	}

	g_mainGPUTarget = gpu_target;


	SDL_SetEventFilter(TemplateHelper_HandleAppEvents, nil);

	LoadTextures();
	LoadShaders();
	InitUniforms();



	/* We use a BlurrrTicker instead of SDL_GetTicks
	   for the game clock because it can allow us to pause or scale time.
	*/
	g_gameClock = BlurrrTicker_Create();
	BlurrrTicker_Start(g_gameClock);
	g_baseTime = BlurrrTicker_UpdateTime(g_gameClock);
	g_lastFrameTime = g_baseTime;

	g_appDone = false;
	
	while(!g_appDone)
	{
		main_loop();
	}

	SDL_SetEventFilter(nil, nil);


	CleanUp();

	ALmixer_Quit();
	TTF_Quit();
	IMG_Quit();
	GPU_Quit();
	SDL_Quit();
	
	return 0;
}



