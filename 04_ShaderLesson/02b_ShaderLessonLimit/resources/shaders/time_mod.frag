#ifdef GL_ES
#	if __VERSION__ >= 130
#		define varying in
		out vec4 vgl_FragColor;
#		define texture2D texture
#	else
#		define vgl_FragColor gl_FragColor
#	endif
	precision mediump float;
#else
#	if __VERSION__ >= 130
#		define varying in
		out vec4 vgl_FragColor;
#		define texture2D texture
#	else
#		define vgl_FragColor gl_FragColor
#	endif
#endif

varying vec2 texCoord;

uniform sampler2D theTexture;

void main(void)
{
	vec4 texfrag = texture2D(theTexture, texCoord);

	if( (texCoord.s > .25) && (texCoord.s < .75) && (texCoord.t > .25) && (texCoord.t < .75))
	{
		vgl_FragColor = texfrag;
	}
	else
	{
		float gray = 0.3*texfrag.r + 0.59*texfrag.g + 0.11*texfrag.b;
		vgl_FragColor = vec4(gray, gray, gray, texfrag.a);
	}
}
