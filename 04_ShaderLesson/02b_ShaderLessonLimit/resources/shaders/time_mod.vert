#ifdef GL_ES
#	if __VERSION__ >= 300
#		define attribute in
#		define varying out
#	endif
	precision lowp float;
#else
#	if __VERSION__ >= 130
#		define attribute in
#		define varying out
#	endif
#endif

attribute vec3 gpu_Vertex;
attribute vec2 gpu_TexCoord;
attribute vec4 gpu_Color;

uniform mat4 gpu_ModelViewProjectionMatrix;

varying vec2 texCoord;

void main(void)
{
	texCoord = vec2(gpu_TexCoord);
	vec4 pos4 = gpu_ModelViewProjectionMatrix * vec4(gpu_Vertex, 1.0);
	gl_Position = pos4;
}
