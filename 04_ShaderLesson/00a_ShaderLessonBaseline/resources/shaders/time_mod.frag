#ifdef GL_ES
#	if __VERSION__ >= 130
#		define varying in
		out vec4 vgl_FragColor;
#		define texture2D texture
#	else
#		define vgl_FragColor gl_FragColor
#	endif
	precision mediump float;
#else
#	if __VERSION__ >= 130
#		define varying in
		out vec4 vgl_FragColor;
#		define texture2D texture
#	else
#		define vgl_FragColor gl_FragColor
#	endif
#endif

varying vec2 texCoord;

uniform sampler2D theTexture;

void main(void)
{
	vgl_FragColor = texture2D(theTexture, texCoord);
}
