#ifdef GL_ES
#	if __VERSION__ >= 130
#		define varying in
		out vec4 vgl_FragColor;
#		define texture2D texture
#	else
#		define vgl_FragColor gl_FragColor
#	endif
	precision mediump float;
#else
#	if __VERSION__ >= 130
#		define varying in
		out vec4 vgl_FragColor;
#		define texture2D texture
#	else
#		define vgl_FragColor gl_FragColor
#	endif
#endif

varying vec4 color;
varying vec2 texCoord;

uniform sampler2D tex;
uniform float time;

void main(void)
{
	vec4 fcolor = vec4(0.5*(vec3(1,1,1) + vec3(sin(time), sin(time/5.0), sin(time/3.0))), color.a);
	vgl_FragColor = texture2D(tex, texCoord) * fcolor;
}
