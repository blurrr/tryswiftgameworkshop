
/*
// Recommend trying to avoid Foundation because it is unfinished and big 
// (and may not be included yet in your Swift distribution).
// SDL and BlurrrCore provide most of the C library so it can be avoided.
// Modify your CMakeBlurrrDefaultOptionsOverride.cmake to include these 
// libraries or not. That file controls whether the libraries will be 
// copied into your app bundle.

#if os(Linux)
    import Glibc
#else
    import Darwin
#endif

import Foundation

*/
let MILLESECONDS_PER_FRAME : UInt32 = 16 * 1  /* about 60 frames per second */

let VIRTUAL_SCREEN_WIDTH : Int32 = 1280
let VIRTUAL_SCREEN_HEIGHT : Int32 = 720
let REAL_SCREEN_WIDTH = VIRTUAL_SCREEN_WIDTH
let REAL_SCREEN_HEIGHT = VIRTUAL_SCREEN_HEIGHT


var g_appDone = false;
var g_mainGPUTarget : UnsafeMutablePointer<GPU_Target>? = nil;

var g_baseTime : UInt32 = 0
var g_lastFrameTime : UInt32 = 0
var g_gameClock : OpaquePointer? = nil;
//var g_mainImage : UnsafeMutablePointer<GPU_Image>? = nil;
let MAX_FLAP_IMAGES: Int = 3
var g_flapImage: UnsafeMutablePointer<GPU_Image>? = nil;



let DEFAULT_POSITION_X: Float32 = Float32(VIRTUAL_SCREEN_WIDTH) / 2.0
let DEFAULT_POSITION_Y: Float32 = Float32(VIRTUAL_SCREEN_HEIGHT) / 2.0
let DEFAULT_VELOCITY_X: Float32 = 200.0;
let DEFAULT_VELOCITY_Y: Float32 = 0.0;
let DEFAULT_FLAP_ANIMATION_DURATION_MS: Int = 1000
let DEFAULT_NUMBER_OF_FRAME_IMAGES: Int = 3

func ComputeLinearInterpolation(_ current_value: Float, start_value: Float, end_value: Float) -> Float
{
	/* Use the linear interpolation formula:
	* X = (1-t)x0 + tx1
	* where x0 would be the start value
	* and x1 is the final value
	* and t is delta_time*inv_time (adjusts 0 <= time <= 1)
	* delta_time = current_time-start_time
	* inv_time = 1/ (end_time-start_time)
	* so t = current_time-start_time / (end_time-start_time)
	*
	*/
	if(current_value > end_value)
	{
		return 1.0;
	}
	else if(current_value < start_value)
	{
		return 0.0;
	}
	return (current_value-start_value) / (end_value-start_value);
}

struct Sprite
{
	var xPos: Float32 = DEFAULT_POSITION_X
	var yPos: Float32 = DEFAULT_POSITION_Y
	var xVel: Float32 = DEFAULT_VELOCITY_X
	var yVel: Float32 = DEFAULT_VELOCITY_Y
	var flapImageIndex: Int = 0;
	var flapAnimationDuration: Int = DEFAULT_FLAP_ANIMATION_DURATION_MS;
	var flapStartTime: Int = 0;
	let numberOfFrameImages: Int = DEFAULT_NUMBER_OF_FRAME_IMAGES
	
	mutating func UpdateSpriteFrame(current_time:Uint32)
	{
		let flap_start_time: Int = self.flapStartTime;
		let animation_duration_ms: Int = self.flapAnimationDuration;
		
		let diff_time: Int = Int(current_time) - flap_start_time
		let mod_time = diff_time % animation_duration_ms

		let number_of_frame_images: Int = self.numberOfFrameImages

		
		let linear_interp = ComputeLinearInterpolation(Float(mod_time), start_value:Float(flap_start_time), end_value:Float(flap_start_time+animation_duration_ms));
		var texture_index: Int = Int((linear_interp * Float(number_of_frame_images-1)) + 0.5);
		if(texture_index >= number_of_frame_images)
		{
			texture_index = 0;
		}


		self.flapImageIndex = texture_index;
		
	}


};
var g_mainSprite: Sprite = Sprite();


var g_isPaused: Bool = false;
var g_speedFactor: Float = 1.0;

/* HACK for Android: This is a workaround for Android which doesn't actually
   quit properly. Instead it leaves global and static variables in memory,
   and when the program is relaunched, it doesn't reinitialize these variables.
   So they contain garbage values from the previous run.
   This function force reinitializes global variables.
*/
func InitGlobalVariables()
{
	g_appDone = false
	g_mainGPUTarget = nil

	g_baseTime = 0
	g_lastFrameTime = 0
	g_gameClock = nil;
	
	g_flapImage = nil;


	g_mainSprite.xPos = DEFAULT_POSITION_X;
	g_mainSprite.yPos = DEFAULT_POSITION_Y;
	g_mainSprite.xVel = DEFAULT_VELOCITY_X;
	g_mainSprite.yVel = DEFAULT_VELOCITY_Y;
	g_mainSprite.flapImageIndex = 0;
	g_mainSprite.flapAnimationDuration = DEFAULT_FLAP_ANIMATION_DURATION_MS;
	g_mainSprite.flapStartTime = 0;
	
	g_isPaused = false;
	g_speedFactor = 1.0
}


func SDL_LogV(_ fmt: String, _ arguments: CVarArg...) -> Void
{
	return withVaList(arguments)
	{
		SDL_LogMessageV(
			Int32(SDL_LOG_CATEGORY_APPLICATION),
			SDL_LOG_PRIORITY_INFO,
			fmt,
			$0
		)
	}
}

func SDL_Log(_ strings: String...) -> Void
{
	var whole_string: String = "";
	for str in strings
	{
		whole_string = whole_string + str;
	}
	print(whole_string);
	SDL_LogV(whole_string);
}

func GetResourceDirectoryString() -> String
{
	let base_path = BlurrrPath_CreateResourceDirectoryString();
	if(nil == base_path)
	{
		return ""
	}
	else
	{
		let return_path = String(cString: base_path!);
		BlurrrCore_Free(base_path);
		return return_path;
	}
}

func TemplateHelper_ToggleFullScreen(_ the_target:UnsafeMutablePointer<GPU_Target>) -> Bool
{
	let is_currently_fullscreen = GPU_GetFullscreen();
	let ret_val: Bool;

	if(is_currently_fullscreen)
	{
		ret_val = GPU_SetFullscreen(false, false);
	}
	else
	{
		ret_val = GPU_SetFullscreen(true, true);
	}

	/* The values should be different if it worked. So it is an error if they are the same. */
	if(ret_val == is_currently_fullscreen)
	{
		return false;
	}

	/* Must update viewport first, then set virtual resolution or it doesn't work right. */
	GPU_EXT_UpdateLogicalViewport(the_target, VIRTUAL_SCREEN_WIDTH, VIRTUAL_SCREEN_HEIGHT, 0);
	GPU_SetVirtualResolution(the_target, Uint16(VIRTUAL_SCREEN_WIDTH), Uint16(VIRTUAL_SCREEN_HEIGHT));

    return true;
}



func LoadTextures()
{
	let base_path = GetResourceDirectoryString();

	do
	{
		let resource_file_path = base_path + "fly_atlas.png"
		let image_surface : UnsafeMutablePointer<SDL_Surface>? = IMG_Load(resource_file_path);
		if(nil == image_surface)
		{
			SDL_Log("Failed to load image");
		}
		else
		{
			let the_image = GPU_CopyImageFromSurface(image_surface);
			SDL_FreeSurface(image_surface);
			if(nil == the_image)
			{
				SDL_Log("could not load image");
			}
			
			g_flapImage = the_image;
		}
	}
}

func UnloadTextures()
{
	GPU_FreeImage(g_flapImage);
	g_flapImage = nil;
}


func main_loop()
{
	let start_frame : UInt32 = BlurrrTicker_UpdateTime(g_gameClock);
	let current_time = start_frame;
	let delta_time = current_time - g_lastFrameTime;
//	println("start:\(start_frame), current:\(current_time), delta:\(delta_time)")
	var the_result : CInt = 0;





	// When ALmixer is compiled as multithreaded, this call becomes a no-op.
	// But in case you have a platform that is single-threaded, you want to keep this call.
	ALmixer_Update();


	repeat
	{
		var the_event : SDL_Event = SDL_Event();
		
		the_result = SDL_PollEvent(&the_event)
		if(the_result > 0)
		{
			let event_type: SDL_EventType = SDL_EventType(the_event.type);
			
			switch(event_type.rawValue)
			{
				case SDL_MOUSEMOTION.rawValue:
					SDL_Log("SDL_MOUSEMOTION: \(the_event.button.x) \(the_event.button.y)");
					break;
				case SDL_MOUSEBUTTONDOWN.rawValue:
					SDL_Log("SDL_MOUSEBUTTONDOWN: \(the_event.button.x) \(the_event.button.y)");

				case SDL_MOUSEBUTTONUP.rawValue:
					SDL_Log("SDL_MOUSEBUTTONUP: \(the_event.button.x) \(the_event.button.y)");
				
				case SDL_MOUSEWHEEL.rawValue:
					SDL_Log("SDL_MOUSEBUTTONUP: \(the_event.wheel.x) \(the_event.wheel.y)");

				case SDL_MULTIGESTURE.rawValue:
					if(SDL_fabs(Double(the_event.mgesture.dDist) ) > 0.002)
					{
						SDL_Log("SDL_MULTIGESTURE (bigger): \(the_event.mgesture.dDist)");
					}
					else
					{
						SDL_Log("SDL_MULTIGESTURE: (smaller): \(the_event.mgesture.dDist)");
					}
				
				
				
				case SDL_QUIT.rawValue:
					g_appDone = true;
				case SDL_APP_TERMINATING.rawValue:
					g_appDone = true;
				case SDL_KEYDOWN.rawValue:
					if(SDL_Keycode(SDLK_AC_BACK) == the_event.key.keysym.sym)
					{
						g_appDone = true;
					}
					else if(SDL_Keycode(SDLK_ESCAPE) == the_event.key.keysym.sym)
					{
						g_appDone = true;
					}

					else if(SDL_Keycode(SDLK_RETURN) == the_event.key.keysym.sym)
					{
						/* CMD-Enter (Mac) or Ctrl-Enter (everybody else) to toggle fullscreen */
						#if os(OSX)
							if( (UInt16(the_event.key.keysym.mod) & UInt16(KMOD_GUI)) > 0)
							{
								_ = TemplateHelper_ToggleFullScreen(g_mainGPUTarget!);
							}
						#else
							if( (UInt16(the_event.key.keysym.mod) & UInt16(KMOD_ALT)) > 0)
							{
								_ = TemplateHelper_ToggleFullScreen(g_mainGPUTarget);
							}
						#endif
					}
					/* CMD-F (Mac) or Ctrl-F (everybody else) to toggle fullscreen */
					else if(SDL_Keycode(SDLK_f) == the_event.key.keysym.sym)
					{
						#if os(OSX)
							if( (UInt16(the_event.key.keysym.mod) & UInt16(KMOD_GUI)) > 0)
							{
								_ = TemplateHelper_ToggleFullScreen(g_mainGPUTarget!);
							}
						#else
							if( (UInt16(the_event.key.keysym.mod) & UInt16(KMOD_ALT)) > 0)
							{
								_ = TemplateHelper_ToggleFullScreen(g_mainGPUTarget);
							}
						#endif
					}


					else if( (SDL_Keycode(SDLK_EQUALS) == the_event.key.keysym.sym) || (SDL_Keycode(SDLK_PLUS) == the_event.key.keysym.sym))
					{
						g_mainSprite.xVel += 10.0
					}
					else if(SDL_Keycode(SDLK_MINUS) == the_event.key.keysym.sym)
					{
						g_mainSprite.xVel -= 10.0

					}


					else if(SDL_Keycode(SDLK_a) == the_event.key.keysym.sym)
					{
						SDL_Log("Pressed a");
					}
					else if(SDL_Keycode(SDLK_SPACE) == the_event.key.keysym.sym)
					{
						SDL_Log("Pressed spacebar");
					}
					else if(SDL_Keycode(SDLK_0) == the_event.key.keysym.sym)
					{
						let speed_factor: Float = 0.5
						BlurrrTicker_SetSpeed(g_gameClock, Double(speed_factor))
						g_speedFactor = speed_factor
					}
					else if(SDL_Keycode(SDLK_1) == the_event.key.keysym.sym)
					{
						let speed_factor: Float = 1.0
						BlurrrTicker_SetSpeed(g_gameClock, Double(speed_factor))
						g_speedFactor = speed_factor
					}
					else if(SDL_Keycode(SDLK_2) == the_event.key.keysym.sym)
					{
						let speed_factor: Float = 2.0
						BlurrrTicker_SetSpeed(g_gameClock, Double(speed_factor))
						g_speedFactor = speed_factor
					}
					else if(SDL_Keycode(SDLK_p) == the_event.key.keysym.sym)
					{
						if(!g_isPaused)
						{
							BlurrrTicker_SetSpeed(g_gameClock, 0.0)
							g_isPaused = true;
						}
						else
						{
							BlurrrTicker_SetSpeed(g_gameClock, Double(g_speedFactor))
							g_isPaused = false;
						}
					}
				case SDL_WINDOWEVENT.rawValue:
					switch(the_event.window.event)
					{
						case UInt8(SDL_WINDOWEVENT_HIDDEN.rawValue):
							break;

						case UInt8(SDL_WINDOWEVENT_MINIMIZED.rawValue):
							break;
						case UInt8(SDL_WINDOWEVENT_RESIZED.rawValue):
							GPU_SetWindowResolution(Uint16(the_event.window.data1), Uint16(the_event.window.data2));
							// Must update viewport first, then set virtual resolution or it doesn't work right.
							GPU_EXT_UpdateLogicalViewport(g_mainGPUTarget, VIRTUAL_SCREEN_WIDTH, VIRTUAL_SCREEN_HEIGHT, 0);
							GPU_SetVirtualResolution(g_mainGPUTarget, Uint16(VIRTUAL_SCREEN_WIDTH), Uint16(VIRTUAL_SCREEN_HEIGHT));
							break;
						default:
							break;
					}
					break;	
					
				default:
					break;
			}
		}
	
		
	} while(the_result > 0)
	
	
	Update(g_baseTime, current_time: current_time, delta_time: delta_time)

	Render(g_baseTime, current_time: current_time, delta_time: delta_time)
	g_lastFrameTime = current_time;

}


// We want to modify the actual object, not copies of them, so we pass by reference via inout
func UpdateSpritePosition(_ dt:Float32, sprite:inout Sprite)
{
	let x_pos = sprite.xPos;
	let y_pos = sprite.yPos;
	let x_vel = sprite.xVel;
	let y_vel = sprite.yVel;
	
	var new_x = x_pos + x_vel * dt;
	var new_y = y_pos + y_vel * dt;
	
	if(new_x > Float32(VIRTUAL_SCREEN_WIDTH))
	{
		new_x = new_x.remainder(dividingBy:Float32(VIRTUAL_SCREEN_WIDTH))
		
	}
	if(new_y > Float32(VIRTUAL_SCREEN_HEIGHT))
	{
		new_y = new_y.remainder(dividingBy:Float32(VIRTUAL_SCREEN_HEIGHT))
	}

	sprite.xPos = new_x;
	sprite.yPos = new_y;
}




func Update(_ base_time:Uint32, current_time:Uint32, delta_time:Uint32)
{
	let dt : Float32 = Float32(delta_time)/1000.0;
	
	UpdateSpritePosition(dt, sprite:&g_mainSprite)
	g_mainSprite.UpdateSpriteFrame(current_time:current_time)

	
}


func Render(_ base_time:Uint32, current_time:Uint32, delta_time:Uint32)
{
	//GPU_ClearRGBA(g_mainGPUTarget, 255, 0, 0, 255);
	GPU_Clear(g_mainGPUTarget);
	

	var atlas_coords = [
		GPU_Rect(x:0, y:0, w:60, h:42),
		GPU_Rect(x:61, y:0, w:60, h:42),
		GPU_Rect(x:121, y:0, w:60, h:42),
	]

	GPU_Blit(g_flapImage, &atlas_coords[g_mainSprite.flapImageIndex], g_mainGPUTarget, g_mainSprite.xPos, g_mainSprite.yPos)


	GPU_Flip(g_mainGPUTarget);
}

let TemplateHelper_HandleAppEvents : @convention(c) (UnsafeMutableRawPointer?, UnsafeMutablePointer<SDL_Event>?) -> CInt =
{
	(user_data, the_event) -> CInt in
	
	//	var event_type = SDL_EXT_EventGetTypeFromPtr(the_event);
	var event_type:SDL_EventType = SDL_EventType(the_event!.pointee.type);
	
	switch(event_type)
	{
	case SDL_APP_TERMINATING:
		/* Terminate the app.
		Shut everything down before returning from this function.
		*/
		
		return 0;
	case SDL_APP_LOWMEMORY:
		/* You will get this when your app is paused and iOS wants more memory.
		Release as much memory as possible.
		*/
		return 0;
	case SDL_APP_WILLENTERBACKGROUND:
		/* Prepare your app to go into the background.  Stop loops, etc.
		This gets called when the user hits the home button, or gets a call.
		*/
		
		return 0;
	case SDL_APP_DIDENTERBACKGROUND:
		/* This will get called if the user accepted whatever sent your app to the background.
		If the user got a phone call and canceled it, you'll instead get an    SDL_APP_DIDENTERFOREGROUND event and restart your loops.
		When you get this, you have 5 seconds to save all your state or the app will be terminated.
		Your app is NOT active at this point.
		*/
		return 0;
	case SDL_APP_WILLENTERFOREGROUND:
		/* This call happens when your app is coming back to the foreground.
		Restore all your state here.
		*/
		return 0;
	case SDL_APP_DIDENTERFOREGROUND:
		/* Restart your loops here.
		Your app is interactive and getting CPU again.
		*/
		return 0;
	default:
		/* No special processing, add it to the event queue */
		return 1;
	}
}



func CleanUp()
{
	UnloadTextures();
	BlurrrTicker_Free(g_gameClock);
	g_gameClock = nil;
}




// This function is the official starting point of the Swift program.
func BlurrrMain() -> Int32
{
	// hack for android
	InitGlobalVariables()

	if(SDL_Init(Uint32(SDL_INIT_VIDEO)) < 0)
	{
		print("Could not initialize SDL");
	}
	if(IMG_Init(Int32(IMG_INIT_JPG.rawValue | IMG_INIT_PNG.rawValue | IMG_INIT_TIF.rawValue)) < 0)
	{
		print("Could not initialize SDL_image");
	}
	if(TTF_Init() < 0)
	{
		print("Could not initialize SDL_ttf");
	}
	
	if(ALmixer_Init(0, 0, 0) == ALboolean(AL_FALSE))
	{
		print("Could not initialize ALmixer");
	}
	
	
	let gpu_target: UnsafeMutablePointer<GPU_Target>? = GPU_Init(UInt16(VIRTUAL_SCREEN_WIDTH), UInt16(VIRTUAL_SCREEN_HEIGHT),
		UInt32(GPU_DEFAULT_INIT_FLAGS)
		| UInt32(SDL_WINDOW_RESIZABLE.rawValue)
//		| UInt32(SDL_WINDOW_FULLSCREEN.rawValue)
	);
	if(nil == gpu_target)
	{
		SDL_Log("GPU_Init failed");
		return -1;
	}

	g_mainGPUTarget = gpu_target;


	SDL_SetEventFilter(TemplateHelper_HandleAppEvents, nil);

	LoadTextures();


	/* We use a BlurrrTicker instead of SDL_GetTicks
	   for the game clock because it can allow us to pause or scale time.
	*/
	g_gameClock = BlurrrTicker_Create();
	BlurrrTicker_Start(g_gameClock);
	g_baseTime = BlurrrTicker_UpdateTime(g_gameClock);
	g_lastFrameTime = g_baseTime;

	g_mainSprite.flapStartTime = Int(g_baseTime);

	g_appDone = false;
	
	while(!g_appDone)
	{
		main_loop();
	}

	SDL_SetEventFilter(nil, nil);


	CleanUp();

	ALmixer_Quit();
	TTF_Quit();
	IMG_Quit();
	GPU_Quit();
	SDL_Quit();
	
	return 0;
}



