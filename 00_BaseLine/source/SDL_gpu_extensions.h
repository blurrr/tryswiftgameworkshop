#ifndef BLURRR_GPU_EXTENSIONS_H
#define BLURRR_GPU_EXTENSIONS_H

/**
This file contains some helper functions for SDL_gpu which are not currently part
of the official API.
*/

#ifndef _SDL_GPU_H__
#include "SDL_gpu.h"
#endif

/**
Loads a shader and prepends version/compatibility info before compiling it.
Normally, you can just use GPU_LoadShader() for shader source files or GPU_CompileShader() for strings.
This was adapted from a SDL_gpu example where
'some hardware (certain ATI/AMD cards) does not let you put non-#version preprocessing at the top of the file'
'Therefore, I need to prepend the version info here so I can support both GLSL and GLSLES with one shader file.'
This extends the idea slightly and generate the version based on the current GL version in use.
Milage may vary since OpenGL makes it a habit to make hard breaking changes every version.
For the most future-proof code, you are probably better off always initializing a specific OpenGL version
and loading a version tailored shader yourself instead of using this function.
@param shader_type The shader type, typically GPU_VERTEX_SHADER or GPU_FRAGMENT_SHADER.
@param file_name The shader file to load from. This should typically include the path returned by BlurrrCore_GetResourceDirectoryString() or SDL_GetBasePath().
@return Returns the shader id.
*/
extern Uint32 GPU_EXT_LoadVersionFlexibleShaderFromFile(GPU_ShaderEnum shader_type, const char* file_name);


/**
 Loads a shader and prepends version/compatibility info before compiling it.
 Normally, you can just use GPU_LoadShader() for shader source files or GPU_CompileShader() for strings.
 This was adapted from a SDL_gpu example where
 'some hardware (certain ATI/AMD cards) does not let you put non-#version preprocessing at the top of the file'
 'Therefore, I need to prepend the version info here so I can support both GLSL and GLSLES with one shader file.'
 This extends the idea slightly and generate the version based on the current GL version in use.
 Milage may vary since OpenGL makes it a habit to make hard breaking changes every version.
 For the most future-proof code, you are probably better off always initializing a specific OpenGL version
 and loading a version tailored shader yourself instead of using this function.
 @param shader_type The shader type, typically GPU_VERTEX_SHADER or GPU_FRAGMENT_SHADER.
 @param input_shader_string A string containing your shader program.
 @return Returns the shader id.
*/
extern Uint32 GPU_EXT_LoadVersionFlexibleShaderFromString(GPU_ShaderEnum shader_type, const char* input_shader_string);


/**
 This is similar to SDL_RenderSetLogicalSize which provides letterboxing and overscan modes.
 @param gpu_target The GPU_target to use.
 @param logical_width The logical or virtual width you are pretending you resolution is set to, (e.g. you are pretending you are 640x480 on a 1920x1080 monitor).
 @param logical_height The logical or virtual height you are pretending you resolution is set to, (e.g. you are pretending you are 640x480 on a 1920x1080 monitor).
 @param scale_policy 0 for letterbox, 1 for overscan.
 */
extern void GPU_EXT_UpdateLogicalViewport(GPU_Target* gpu_target, int logical_width, int logical_height, int scale_policy);

/**
 Similar to GPU_GetVirtualCoords, but intended to handle letterboxing/overscan via GPU_EXT_UpdateLogicalViewport.
 @param gpu_target The GPU_target to use.
 @param x OUT value: returns the x coordinate via pass-by-reference.
 @param y OUT value: returns the y coordinate via pass-by-reference.
 @param display_x The x coordinate you want to convert. Typically the SDL_point value you get from an mouse/touch SDL_Event.
 @param display_y The y coordinate you want to convert. Typically the SDL_point value you get from an mouse/touch SDL_Event.
 @param logical_width The logical or virtual width you are pretending you resolution is set to, (e.g. you are pretending you are 640x480 on a 1920x1080 monitor).
 @param logical_height The logical or virtual height you are pretending you resolution is set to, (e.g. you are pretending you are 640x480 on a 1920x1080 monitor).
 @param scale_policy 0 for letterbox, 1 for overscan.
 @note Ideally, these get baked into SDL_gpu and the last 3 parameters can be tracked via internal state making these go away.
 */
extern void GPU_EXT_GetLogicalCoords(GPU_Target* gpu_target, float* x, float* y, float display_x, float display_y, int logical_width, int logical_height, int scale_policy);

#endif /* BLURRR_GPU_EXTENSIONS_H */

